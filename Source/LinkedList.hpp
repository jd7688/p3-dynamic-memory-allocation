//
//  LinkedList.hpp
//  CommandLineTool
//
//  Created by Joseph D'Souza on 06/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef LinkedList_hpp
#define LinkedList_hpp

#include <stdio.h>

class linkedList
{
public:
    linkedList();
    ~linkedList();
    void add (float itemValue);
    float get(int index);
    int size();
    
private:
    struct Node{
        float value;
        Node* next;
    };
    
    Node* head;
};

#endif /* LinkedList_hpp */
