//
//  dynamicArray.hpp
//  CommandLineTool
//
//  Created by Joseph D'Souza on 06/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef dynamicArray_hpp
#define dynamicArray_hpp

#include <stdio.h>
#include <iostream>

class Array
{
public:
    Array();
    ~Array();
    void add (float itemValue);
    void remove (int index);
    float get (int index);
    int size();
    
private:
    int iAsize;
    float* aPointer = nullptr;
};

#endif /* dynamicArray_hpp */
